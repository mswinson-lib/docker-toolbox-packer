.PHONY: build

REPO=mswinson
IMAGENAME=toolbox-packer
IMAGEID=$(REPO)/$(IMAGENAME)
VERSION=latest

build:
	docker build -t $(IMAGEID):$(VERSION) .

deploy:
	docker login --username $(DOCKER_USER) --password $(DOCKER_PASS)
	docker push $(IMAGEID):$(VERSION)

smoke: build
	docker run --rm -v $(PWD):/var/workspace $(IMAGEID):$(VERSION) ./test/smoke.sh

