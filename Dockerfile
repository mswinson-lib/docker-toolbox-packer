FROM hashicorp/packer

RUN mkdir -p /var/tmp/packer-build
ADD source /var/tmp/packer-build
RUN chmod u+x /var/tmp/packer-build/setup.sh && \
    /var/tmp/packer-build/setup.sh

RUN mkdir /var/workspace
WORKDIR /var/workspace

ENTRYPOINT ["/bin/sh", "-c"]
CMD /bin/sh
VOLUME /var/workspace
