#!/usr/bin/env bash
set -e
for f in /var/tmp/packer-build/scripts/*
do
  chmod u+x $f && $f
done

