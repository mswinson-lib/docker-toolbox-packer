#!/usr/bin/env bash
apk add --update make
apk add --update curl
apk add --update jq
apk add --update docker
